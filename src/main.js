import Vue from 'vue'

// Dependencies
import WebFont from 'webfontloader'
import svg4everybody from 'svg4everybody'
import objectFitImages from 'object-fit-images'

// Root Vue instance
import App from './App.vue'

// Gsap Animations
import GsapAnimation from './components/GsapAnimation'
import GsapAnimationItem from './components/GsapAnimationItem'

Vue.component('GsapAnimation', GsapAnimation)
Vue.component('GsapAnimationItem', GsapAnimationItem)

// Webfont loader
WebFont.load({
  google: {
    families: [
      'PT Sans:400,700',
      'PT Serif:400,700',
    ],
  },
})

// Pollyfills
svg4everybody()
objectFitImages()

Vue.config.productionTip = false

new Vue(App).$mount('#app')
