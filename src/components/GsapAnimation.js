import wrapper from '@/util/wrapper'
import propsTag from '@/util/propsTag'

import BaseIntersection from './BaseIntersection'

export default {
  provide() {
    return {
      [wrapper]: this,
    }
  },
  props: {
    ...propsTag,
    persist: {
      type: Boolean,
      default: true,
    },
  },

  data: () => ({
    previousIsIntersecting: undefined,
  }),

  methods: {
    handleCallback({ isIntersecting }) {
      const { previousIsIntersecting } = this
      if (previousIsIntersecting === undefined) {
        this.previousIsIntersecting = isIntersecting
        return
      }

      if (isIntersecting === previousIsIntersecting) {
        return
      }

      if (this.persist === !isIntersecting) {
        return
      }

      this.previousIsIntersecting = isIntersecting
    },
  },
  render(createElement) {
    const elementData = { props: { tag: this.tag, callback: this.handleCallback } }

    return createElement(BaseIntersection, elementData, this.$slots.default)
  },
}
