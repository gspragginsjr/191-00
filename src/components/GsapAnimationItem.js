import root from '@/util/root'
import wrapper from '@/util/wrapper'
import propstag from '@/util/propsTag'

import BezierEasing from 'bezier-easing'
import { TimelineMax, Ease } from 'gsap'

const typeCheckNumber = { type: Number, default: 0 }
const typeCheckString = { type: String, default: '' }
const typeCheckBoolean = { type: Boolean, default: false }

export default {
  inject: { wrapper, root },
  props: {
    ...propstag,
    delay: typeCheckNumber,
    duration: typeCheckNumber,
    hasMask: typeCheckBoolean,
    noIntersect: typeCheckBoolean,
    animationType: typeCheckString,
    backgroundColor: typeCheckString,
  },

  data: () => ({
    timeline: new TimelineMax(),
    ease: new Ease(BezierEasing(0.55, 0.085, 0, 0.99)),
  }),

  mounted() {
    this[this.animationType]()
  },

  methods: {
    fadeInUp() {
      const { delay, $el } = this
      const { long } = this.root.durations

      this.timeline.from(
        $el,
        long,
        {
          y: 100,
          opacity: 0,
        },
        delay,
      ).to(
        $el,
        long,
        {
          y: 0,
          opacity: 1,
          ease: this.ease,
        },
        delay,
      )
    },

    fadeInLeft() {
      const { ease, $el } = this
      const { base } = this.root.durations

      this.timeline.to(
        $el,
        base,
        {
          x: 0,
          ease,
        },
      )

      this.timeline.to(
        $el.children[0],
        base,
        {
          xPercent: -100,
          ease,
        },
        (this.delay + base),
      )
    },

    fadeInRight() {
      const { ease, $el } = this
      const { base } = this.root.durations

      this.timeline.to(
        $el,
        base,
        {
          x: 0,
          ease,
        },
      )

      this.timeline.to(
        $el.children[0],
        base,
        {
          xPercent: 100,
          ease,
        },
        (this.delay + base),
      )
    },

    handleIsIntersecting(intersecting) {
      const { timeline } = this

      return intersecting ? timeline.play() : timeline.pause(0)
    },
  },

  watch: {
    'wrapper.previousIsIntersecting': 'handleIsIntersecting',
  },

  render(createElement) {
    const maskElementData = {
      style: {
        position: 'absolute',
        top: 0,
        'z-index': 2,
        width: '100%',
        height: '100%',
        'background-color': this.backgroundColor,
      },
    }
    const hasMask = this.hasMask ? createElement('div', maskElementData) : undefined

    return createElement(this.tag, [hasMask, this.$slots.default])
  },
}
