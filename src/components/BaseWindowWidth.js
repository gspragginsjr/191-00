export default {
  props: {
    tag: {
      type: String,
      default: 'div',
    },
  },
  data() {
    return {
      windowWidth: window.innerWidth,
    }
  },

  mounted() {
    window.addEventListener('resize', () => this.setWindowWidth())
  },


  beforeDestroy() {
    window.removeEventListener('resize', this.setWindowWidth)
  },

  methods: {
    setWindowWidth() {
      this.windowWidth = window.innerWidth
    },
  },

  render(createElement) {
    return createElement(this.tag,
      this.$scopedSlots.default({ windowWidth: this.windowWidth }),
    )
  },
}
