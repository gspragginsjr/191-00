import propsTag from '@/util/propsTag'
import propsCallback from '@/util/propsCallback'

export default {
  props: {
    ...propsTag,
    ...propsCallback,
    options: {
      type: Object,
      default: () => ({
        rootMargin: '-20%',
      }),
    },
  },

  data: () => ({
    intersectionObserver: undefined,
  }),

  watch: {
    options: 'reset',
    callback: 'reset',
  },
  mounted() {
    this.create()
    this.observe()
  },
  methods: {
    create() {
      this.intersectionObserver = new IntersectionObserver(
        ([entry]) => this.callback(entry),
        this.options,
      )
    },
    observe() {
      this.intersectionObserver.observe(this.$el)
    },
    disconnect() {
      this.intersectionObserver.disconnect()
    },
    reset() {
      this.disconnect()
      this.create()
      this.observe()
    },
  },
  render(createElement) {
    return createElement(this.tag, this.$slots.default)
  },
}
