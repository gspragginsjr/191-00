export default {
  teal: '#1b96ba',
  lime: '#65a328',
  blue: '#3399ff',
  orange: '#f26227',
  cerulean: '#2788c9',
  tangerine: '#ffab30',
}
