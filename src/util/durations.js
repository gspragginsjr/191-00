export default {
  base: 0.55,
  shortest: 0.1,
  shorter: 0.25,
  short: 0.3,
  medium: 0.9,
  long: 1.2,
  longer: 1.5,
  longest: 2.0,
}
