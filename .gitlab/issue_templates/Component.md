## Developer Checklist
To be completed prior to moving ticket to Visual QA phase:

__Desktop Browsers__ (medium & large breakpoints)
  - [ ] Mac Chrome
  - [ ] Mac Firefox
  - [ ] Mac Safari (Mojave)
  - [ ] Mac Safari (High Sierra)
  - [ ] Windows Chrome
  - [ ] Windows Firefox
  - [ ] Windows Edge
  - [ ] Windows IE 11

__Mobile Browsers__
  - [ ] iOS 12 Safari iPad
  - [ ] iOS 12 Safari iPhone
  - [ ] iOS 11 Safari iPad
  - [ ] iOS 11 Safari iPhone
  - [ ] Android Chrome

__Content Stress Test__
  - [ ] Long Words
  - [ ] Long Content
  - [ ] Short Content
  - [ ] Missing Content

__Interaction/Motion__ (if applicable)
  - [ ] Hover/focus/active states

__Accessibility__
  - [ ] Alt text on all necessary images
  - [ ] Semantic HTML tag
  - [ ] Appropriate aria labeling
  - [ ] Keyboard navigable
